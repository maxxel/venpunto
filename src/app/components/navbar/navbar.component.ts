import { Component, OnInit } from '@angular/core';
import{NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  providers: [NgbCarouselConfig]
})
export class NavbarComponent implements OnInit {
  public app_name = "VEN | PUNTO";
  navbarOpen = false;
  constructor(config: NgbCarouselConfig) {
    //asignando propiedades al carrousel
    config.interval =4000;
    config.wrap = true;
    config.keyboard = true;
    config.pauseOnHover = false;
   }

  ngOnInit() {
  }
  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }

}
