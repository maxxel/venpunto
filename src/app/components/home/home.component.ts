import { Component, OnInit } from '@angular/core';
import { trigger, state, transition, animate, style } from '@angular/animations';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [
    trigger('fade', [
      transition('void => *',[
        style({opacity:0}),
        animate(2000)
      ])
    ]),
    trigger('fade2', [
      transition('void => *',[
        style({opacity:0}),
        animate(9000)
      ])
    ])
  ]
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    
  }

}
