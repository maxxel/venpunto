import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ProductsComponent } from './components/products/products.component';
import { PreRegisterComponent } from './components/pre-register/pre-register.component';
import { ContactComponent } from './components/contact/contact.component';
import { WhoWeAreComponent } from './components/who-we-are/who-we-are.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

const routes: Routes = [
  {path:'', component: HomeComponent},
  {path:'products', component:ProductsComponent},
  {path:'pre-register', component:PreRegisterComponent},
  {path:'contact', component:ContactComponent},
  {path:'who-we-are',component:WhoWeAreComponent},
  {path:'**', component:PageNotFoundComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
